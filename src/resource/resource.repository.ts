import { DataSource, Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { Resource } from './entities/resource.entity';

@Injectable()
export class ResourceRepository extends Repository<Resource> {
  constructor(private readonly dataSource: DataSource) {
    super(Resource, dataSource.createEntityManager());
  }

  async findExistingOne(id: string) {
    const target = await this.findOne({ where: { id } });
    if (!target) {
      throw new NotFoundException();
    }

    return target;
  }
}
