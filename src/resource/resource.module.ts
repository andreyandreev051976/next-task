import { Module } from '@nestjs/common';
import { ResourceService } from './resource.service';
import { ResourceRepository } from './resource.repository';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ResourceRepository])],
  providers: [ResourceRepository, ResourceService],
  exports: [ResourceRepository, ResourceService],
})
export class ResourceModule {}
