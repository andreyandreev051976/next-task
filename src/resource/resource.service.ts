import { Injectable } from '@nestjs/common';
import { CreateResourceDto } from './dto/create-resource.dto';
import { UpdateResourceDto } from './dto/update-resource.dto';
import { ResourceRepository } from './resource.repository';

@Injectable()
export class ResourceService {
  constructor(private readonly resourceRepository: ResourceRepository) {}
  async create(createResourceDto: CreateResourceDto) {
    const entity = this.resourceRepository.create(createResourceDto);
    await this.resourceRepository.insert(entity);
    return entity;
  }

  findAll() {
    return this.resourceRepository.find();
  }

  findOne(id: string) {
    return this.resourceRepository.findExistingOne(id);
  }

  async update(id: string, updateResourceDto: UpdateResourceDto) {
    await this.resourceRepository.findExistingOne(id);
    this.resourceRepository.update({ id }, updateResourceDto);
    return this.findOne(id);
  }

  async remove(id: string) {
    await this.resourceRepository.findExistingOne(id);
    await this.resourceRepository.delete(id);
    return true;
  }
}
