import { ApiProperty } from '@nestjs/swagger';

export class CreateResourceDto {
  @ApiProperty()
  get: string;

  @ApiProperty()
  update: string;
}
