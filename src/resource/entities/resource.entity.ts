import { PropertyTemplates } from 'src/property/entities/property-templates.entity';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Resource {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'text',
  })
  get: string;

  @Column({
    type: 'text',
  })
  update: string;

  @OneToOne(() => PropertyTemplates, (property) => property.resource, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn()
  property: PropertyTemplates;
}
