import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { TaskRepository } from './task.repository';
import { PropertyService } from 'src/property/property.service';
import { CreateTaskPropertyDTO } from './dto/create-property.dto';
import { UpdateTaskPropertyDTO } from './dto/update-property.dto';
import { FileClient } from 'src/clients/file/file.client';
import { firstValueFrom } from 'rxjs';
import { MediaService } from 'src/media/media.service';
import { DeleteMediaDTO } from './dto/delete-media.dto';
import { SpaceService } from 'src/space/space.service';

@Injectable()
export class TaskService {
  constructor(
    private readonly taskRepository: TaskRepository,
    private readonly propertyService: PropertyService,
    private readonly fileClient: FileClient,
    private readonly mediaService: MediaService,
    private readonly spaceService: SpaceService,
  ) {}

  async create({
    media,
    spaceId,
    subtasksId = [],
    ...createTaskDto
  }: CreateTaskDto) {
    const filesToUpload = (media || []).map((file) =>
      firstValueFrom(this.fileClient.uploadFile(file)),
    );
    const files = await Promise.all(filesToUpload);
    try {
      const space = await this.spaceService.findOne(spaceId);
      const taskInstance = this.taskRepository.create({
        ...createTaskDto,
        space,
      });
      for (const subtaskId of subtasksId) {
        const subtask = await this.findOne(subtaskId);
        if (subtask) {
          subtask.id = taskInstance.id;
          this.taskRepository.update(subtask.id, subtask);
        }
      }
      await this.taskRepository.insert(taskInstance);
      const filesEntities = files.map((file) =>
        this.mediaService.create({
          mediaId: file.id,
          taskId: taskInstance.id,
        }),
      );
      await Promise.all(filesEntities);

      return taskInstance;
    } catch (error) {
      const filesToDelete = files.map((file) =>
        firstValueFrom(this.fileClient.deleteFile(file.id)),
      );
      await Promise.all(filesToDelete);
      throw error;
    }
  }

  findAll() {
    return this.taskRepository.find({
      relations: { properties: true, media: true },
    });
  }

  findOne(id: string) {
    return this.taskRepository.findOne({
      where: { id },
      relations: { properties: true },
    });
  }

  async update(
    id: string,
    { media, spaceId, subtasksId, ...updateTaskDto }: UpdateTaskDto,
  ) {
    const taskInstance = await this.taskRepository.findExistingOne(id);
    const filesToUpload = (media || []).map((file) =>
      firstValueFrom(this.fileClient.uploadFile(file)),
    );
    const files = await Promise.all(filesToUpload);
    try {
      const space = await this.spaceService.findOne(spaceId);
      await this.taskRepository.update({ id }, { ...updateTaskDto, space });

      for (const subtaskId of subtasksId) {
        const subtask = await this.findOne(subtaskId);
        if (subtask) {
          subtask.id = taskInstance.id;
          this.taskRepository.update(subtask.id, subtask);
        }
      }

      const filesEntities = files.map((file) =>
        this.mediaService.create({
          mediaId: file.id,
          taskId: id,
        }),
      );
      await Promise.all(filesEntities);

      return this.findOne(id);
    } catch (error) {
      const filesToDelete = files.map((file) =>
        firstValueFrom(this.fileClient.deleteFile(file.id)),
      );
      await Promise.all(filesToDelete);
      throw error;
    }
  }

  async remove(id: string) {
    await this.taskRepository.delete({ id });
  }

  async addProperty(id: string, { templateId, ...dto }: CreateTaskPropertyDTO) {
    const task = await this.taskRepository.findExistingOne(id);
    const isExists = await this.propertyService.findOneBy({
      where: { task: { id: task.id }, templateId },
    });
    if (isExists) {
      throw new BadRequestException('Already exists');
    }
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    await this.propertyService.create({ ...dto, templateId, task });
    return await this.findOne(id);
  }

  async updateProperty(
    id: string,
    { propertyId, ...dto }: UpdateTaskPropertyDTO,
  ) {
    await this.propertyService.update(propertyId, dto);
    return await this.findOne(id);
  }

  async deleteProperty(id: string, { propertyId }: UpdateTaskPropertyDTO) {
    const task = await this.findOne(id);
    if (!task) {
      throw new NotFoundException();
    }
    await this.propertyService.remove(propertyId);
    return await this.findOne(id);
  }

  async deleteMedia(id: string, dto: DeleteMediaDTO) {
    await this.taskRepository.findExistingOne(id);
    await this.mediaService.remove(dto.id);
  }
}
