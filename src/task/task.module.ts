import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskController } from './task.controller';
import { TaskRepository } from './task.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TaskHistory } from './entities/task-history.entity';
import { Task } from './entities/task.entity';
import { UserTypeModule } from 'src/user-type/user-type.module';
import { PropertyModule } from 'src/property/property.module';
import { FileModule } from 'src/clients/file/file.module';
import { MediaModule } from 'src/media/media.module';
import { SpaceModule } from 'src/space/space.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Task, TaskHistory]),
    UserTypeModule,
    PropertyModule,
    MediaModule,
    FileModule,
    SpaceModule,
  ],
  controllers: [TaskController],
  providers: [TaskRepository, TaskService],
  exports: [TaskRepository, TaskService],
})
export class TaskModule {}
