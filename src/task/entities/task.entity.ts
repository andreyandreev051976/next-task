import { Media } from 'src/media/entities/media.entity';
import { Property } from 'src/property/entities/property.entity';
import { Space } from 'src/space/entities/space.entity';
import { User } from 'src/user/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Tree,
  TreeChildren,
  TreeParent,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
@Tree('closure-table')
export class Task {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'text',
  })
  name: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  description: string;

  @Column({
    type: 'bool',
    default: false,
  })
  done: boolean;

  @OneToMany(() => Property, (property) => property.task, {
    cascade: true,
    eager: true,
  })
  properties: Property[];

  @OneToMany(() => User, (user) => user.id)
  users: User[];

  @OneToMany(() => Media, (media) => media.taskId)
  media: Media[];

  @ManyToOne(() => Space, (space) => space.tasks, {
    orphanedRowAction: 'nullify',
    onDelete: 'SET NULL',
    onUpdate: 'SET NULL',
  })
  @JoinColumn()
  space: Space;

  @TreeParent()
  parent: Task;

  @TreeChildren()
  children: Task[];

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;
}
