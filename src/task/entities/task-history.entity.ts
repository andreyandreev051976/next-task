import { Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Task } from './task.entity';
import {
  HistoryActionColumn,
  HistoryActionType,
  HistoryFor,
  SnapshotColumn,
  InitiatorColumn,
} from 'src/history';

@Entity()
@HistoryFor(Task)
export class TaskHistory {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @SnapshotColumn({ type: 'jsonb' })
  payload: Task;

  @HistoryActionColumn()
  action: HistoryActionType;

  @UpdateDateColumn()
  updateAt: Date;

  @InitiatorColumn()
  userId: number;
}
