import { ApiProperty } from '@nestjs/swagger';

export class CreateTaskDto {
  @ApiProperty()
  name: string;

  @ApiProperty({
    required: false,
  })
  description: string | null;

  @ApiProperty()
  spaceId: string;

  @ApiProperty()
  subtasksId: string[];

  @ApiProperty({
    type: 'array',
    items: {
      type: 'string',
      format: 'binary',
    },
    required: false,
  })
  media: Express.Multer.File[] | null;
}
