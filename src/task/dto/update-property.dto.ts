import { ApiProperty, OmitType } from '@nestjs/swagger';
import { CreatePropertyDto } from 'src/property/dto/create-property.dto';

export class UpdateTaskPropertyDTO extends OmitType(CreatePropertyDto, [
  'templateId',
]) {
  @ApiProperty()
  propertyId: string;
}
