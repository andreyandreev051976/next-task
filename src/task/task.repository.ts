import { DataSource, Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { Task } from './entities/task.entity';

@Injectable()
export class TaskRepository extends Repository<Task> {
  constructor(private readonly dataSource: DataSource) {
    super(Task, dataSource.createEntityManager());
  }

  async findExistingOne(id: string) {
    const target = await this.findOne({ where: { id } });
    if (!target) {
      throw new NotFoundException();
    }

    return target;
  }
}
