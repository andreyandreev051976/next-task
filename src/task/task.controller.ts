import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseGuards,
  Patch,
  Delete,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { TaskService } from './task.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { Roles } from 'src/roles/roles-auth.decorator';
import { RolesGuard } from 'src/roles/roles.guard';
import { ApiConsumes, ApiTags } from '@nestjs/swagger';
import { UpdateTaskDto } from './dto/update-task.dto';
import { UpdateTaskPropertyDTO } from './dto/update-property.dto';
import { CreateTaskPropertyDTO } from './dto/create-property.dto';
import { FilesInterceptor } from '@nestjs/platform-express';
import { DeleteMediaDTO } from './dto/delete-media.dto';

@ApiTags('task')
@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Post()
  @ApiConsumes('multipart/form-data')
  @Roles('ADMIN')
  @UseGuards(RolesGuard)
  @UseInterceptors(FilesInterceptor('media'))
  create(
    @UploadedFiles() media: Array<Express.Multer.File>,
    @Body() createTaskDto: CreateTaskDto,
  ) {
    return this.taskService.create({ ...createTaskDto, media });
  }

  @Get()
  findAll() {
    return this.taskService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.taskService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTaskDto: UpdateTaskDto) {
    return this.taskService.update(id, updateTaskDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.taskService.remove(id);
  }

  @Post(':id/property/add')
  addProperty(@Param('id') id: string, @Body() dto: CreateTaskPropertyDTO) {
    return this.taskService.addProperty(id, dto);
  }

  @Post(':id/property/update')
  updateProperty(@Param('id') id: string, @Body() dto: UpdateTaskPropertyDTO) {
    return this.taskService.updateProperty(id, dto);
  }

  @Post(':id/property/delete')
  deleteProperty(@Param('id') id: string, @Body() dto: UpdateTaskPropertyDTO) {
    return this.taskService.deleteProperty(id, dto);
  }

  @Post(':id/media/delete')
  deleteMedia(@Param('id') id: string, @Body() dto: DeleteMediaDTO) {
    return this.taskService.deleteMedia(id, dto);
  }
}
