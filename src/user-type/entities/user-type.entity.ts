import { User } from 'src/user/entities/user.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class UserType {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    unique: true,
    type: 'varchar',
    length: 255,
  })
  value: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  name: string;

  @OneToMany(() => User, (user) => user.id)
  users: User[];
}
