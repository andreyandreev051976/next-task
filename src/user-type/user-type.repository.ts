import { DataSource, Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { UserType } from './entities/user-type.entity';

@Injectable()
export class UserTypeRepository extends Repository<UserType> {
  constructor(private readonly dataSource: DataSource) {
    super(UserType, dataSource.createEntityManager());
  }

  async findExistingOne(id: string) {
    const target = await this.findOne({ where: { id } });
    if (!target) {
      throw new NotFoundException();
    }

    return target;
  }

  async getByValue(value: string) {
    const target = await this.findOne({ where: { value } });
    if (!target) {
      throw new NotFoundException();
    }

    return target;
  }
}
