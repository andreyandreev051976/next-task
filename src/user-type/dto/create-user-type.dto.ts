export class CreateUserTypeDto {
  readonly value: string;
  readonly name: string;
}
