import { Injectable } from '@nestjs/common';
import { CreateUserTypeDto } from './dto/create-user-type.dto';
import { UpdateUserTypeDto } from './dto/update-user-type.dto';
import { UserTypeRepository } from './user-type.repository';

@Injectable()
export class UserTypeService {
  constructor(private readonly userTypeRepository: UserTypeRepository) {}
  async create(createUserTypeDto: CreateUserTypeDto) {
    const entity = this.userTypeRepository.create(createUserTypeDto);
    await this.userTypeRepository.insert(entity);
    return entity;
  }

  findAll() {
    return this.userTypeRepository.find();
  }

  findOne(id: string) {
    return this.userTypeRepository.findOne({ where: { id } });
  }

  findByValue(value: string) {
    return this.userTypeRepository.getByValue(value);
  }

  async update(id: string, updateUserTypeDto: UpdateUserTypeDto) {
    await this.userTypeRepository.findExistingOne(id);
    await this.userTypeRepository.update({ id }, updateUserTypeDto);
    return this.findOne(id);
  }

  async remove(id: string) {
    await this.userTypeRepository.findExistingOne(id);
    await this.userTypeRepository.delete({ id });
    return;
  }
}
