import { Task } from 'src/task/entities/task.entity';
import { UserType } from 'src/user-type/entities/user-type.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'task-user' })
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Task, (task) => task.users, {
    orphanedRowAction: 'delete',
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  task: Task;

  @Column({
    type: 'int4',
    nullable: false,
  })
  userId: number;

  @ManyToOne(() => UserType, (userType) => userType.users, {
    orphanedRowAction: 'nullify',
    onDelete: 'SET NULL',
  })
  @JoinColumn()
  userType: UserType;
}
