import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserRepository } from './user.repository';
import { FindOptionsWhere } from 'typeorm';
import { User } from './entities/user.entity';
import { TaskService } from 'src/task/task.service';
import { UserTypeService } from 'src/user-type/user-type.service';
import { USER_KEY } from 'src/app.constants';
import { ClsService } from 'nestjs-cls';
import { JoinUserByUserTypeDTO, JoinUserDTO } from './dto/join-user.dto';
import { AuthClient } from 'src/clients/auth/auth.client';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly taskService: TaskService,
    private readonly userTypeService: UserTypeService,
    private readonly authClient: AuthClient,
    private readonly cls: ClsService,
  ) {}

  private async getUsers(ids: number[]) {
    const usersRequests = ids.map((id) =>
      firstValueFrom(this.authClient.getById(id)),
    );

    return await Promise.all(usersRequests);
  }

  async create(createUserDto: CreateUserDto) {
    const { taskId, userId, userTypeId } = createUserDto;
    const taskEntity = await this.taskService.findOne(taskId);
    const userTypeEntity = await this.userTypeService.findOne(userTypeId);
    const entity = this.userRepository.create({
      task: taskEntity,
      userId,
      userType: userTypeEntity,
    });
    await this.userRepository.insert(entity);
    return entity;
  }

  findAll() {
    return this.userRepository
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.task', 'task')
      .groupBy('user.id')
      .addGroupBy('task.id')
      .getMany();
  }

  findOne(id: string) {
    return this.userRepository.findOne({
      where: { id },
    });
  }

  async findByTaskId(taskId: string) {
    const result = await this.userRepository.find({
      where: { task: { id: taskId } },
      relations: {
        userType: true,
      },
    });

    const usersIds = result.map((user) => user.userId);

    const users = await this.getUsers(usersIds);

    const mappedData = result.map((task, id) => ({
      userType: task.userType,
      user: users[id],
    }));

    return mappedData;
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const { taskId, userId, userTypeId } = updateUserDto;
    await this.userRepository.findExistingOne(id);
    const taskEntity = await this.taskService.findOne(taskId);
    const userTypeEntity = await this.userTypeService.findOne(userTypeId);
    await this.userRepository.update(
      { id },
      { userId, task: taskEntity, userType: userTypeEntity },
    );
    return this.findOne(id);
  }

  async remove(id: string) {
    await this.userRepository.findExistingOne(id);
    await this.userRepository.delete({ id });
    return;
  }

  async removeBy(where: FindOptionsWhere<User>) {
    await this.userRepository.delete(where);
    return;
  }

  async joinUser(dto: JoinUserDTO) {
    this.create(dto);

    return true;
  }

  async joinCurrentUser(dto: JoinUserByUserTypeDTO) {
    const user = this.cls.get(USER_KEY);
    const userTypeEntity = await this.userTypeService.findByValue(dto.userType);
    const hasUser = this.authClient.hasById(user.id);
    if (!hasUser) {
      throw new BadRequestException('no user');
    }
    return this.joinUser({
      taskId: dto.taskId,
      userTypeId: userTypeEntity.id,
      userId: user.id,
    });
  }

  async removeUser(dto: JoinUserDTO) {
    await this.removeBy(dto);
    return true;
  }

  async removeCurrentUser(dto: JoinUserByUserTypeDTO) {
    const user = this.cls.get(USER_KEY);
    const userTypeEntity = await this.userTypeService.findByValue(dto.userType);
    const hasUser = this.authClient.hasById(user.id);
    if (!hasUser) {
      throw new BadRequestException('no user');
    }
    return this.removeUser({
      taskId: dto.taskId,
      userTypeId: userTypeEntity.id,
      userId: user.id,
    });
  }
}
