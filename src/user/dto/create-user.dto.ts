export class CreateUserDto {
  readonly taskId: string;
  readonly userId: number;
  readonly userTypeId: string;
}
