export class JoinUserDTO {
  readonly userId: number;
  readonly taskId: string;
  readonly userTypeId: string;
}

export class JoinUserByTypeValueDTO {
  readonly userId: number;
  readonly taskId: string;
  readonly userType: string;
}

export class JoinUserByUserTypeDTO {
  readonly taskId: string;
  readonly userType: string;
}
