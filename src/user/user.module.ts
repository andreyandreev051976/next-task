import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { UserRepository } from './user.repository';
import { TaskModule } from 'src/task/task.module';
import { UserTypeModule } from 'src/user-type/user-type.module';
import { AuthModule } from 'src/clients/auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([User]),
    TaskModule,
    UserTypeModule,
    AuthModule,
  ],
  controllers: [UserController],
  providers: [UserService, UserRepository],
  exports: [UserService, UserRepository],
})
export class UserModule {}
