import { DataSource, Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { User } from './entities/user.entity';

@Injectable()
export class UserRepository extends Repository<User> {
  constructor(private readonly dataSource: DataSource) {
    super(User, dataSource.createEntityManager());
  }

  async findExistingOne(id: string) {
    const target = await this.findOne({ where: { id } });
    if (!target) {
      throw new NotFoundException();
    }

    return target;
  }
}
