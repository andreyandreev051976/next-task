import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Auth } from 'src/roles/authorized.decorator';
import { JoinUserByUserTypeDTO, JoinUserDTO } from './dto/join-user.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('task-user')
@Controller('task-user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @Get()
  findAll() {
    return this.userService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.userService.findOne(id);
  }

  @Get('/task/:id')
  findByTaskId(@Param('id') id: string) {
    return this.userService.findByTaskId(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userService.remove(id);
  }

  @Post('task/join-user/current')
  @Auth()
  joinCurrentUser(@Body() dto: JoinUserByUserTypeDTO) {
    return this.userService.joinCurrentUser(dto);
  }

  @Post('task/remove-user/current')
  @Auth()
  removeCurrentUser(@Body() dto: JoinUserByUserTypeDTO) {
    return this.userService.removeCurrentUser(dto);
  }

  @Post('task/join-user')
  @Auth()
  joinUser(@Body() dto: JoinUserDTO) {
    return this.userService.joinUser(dto);
  }

  @Post('task/remove-user')
  @Auth()
  removeUser(@Body() dto: JoinUserDTO) {
    return this.userService.removeUser(dto);
  }
}
