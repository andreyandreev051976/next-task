import { Global, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Variables } from './variables.service';

@Global()
@Module({
  imports: [ConfigModule],
  providers: [ConfigService, Variables],
  exports: [Variables],
})
export class VariablesModule {}
