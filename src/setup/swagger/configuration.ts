import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ACCESS_TOKEN_KEY } from 'src/roles/roles.guard';

function setupSwagger(app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle('Service name')
    .setDescription('Service description')
    .setVersion('1.0')
    .addCookieAuth(ACCESS_TOKEN_KEY, {
      type: 'http',
      in: 'Header',
      scheme: 'Bearer',
    })
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/swagger', app, document);
}

export default setupSwagger;
