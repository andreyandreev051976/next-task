import { plainToInstance } from 'class-transformer';
import { validateSync } from 'class-validator';

import { IsNumberString, IsString } from 'class-validator';
import { Environment } from './environment';

class EnvironmentVariables implements NodeJS.ProcessEnv {
  [key: string]: string;
  TZ?: string;
  @IsNumberString()
  DB_PORT: string;

  @IsString()
  DB_HOST: string;

  @IsString()
  DB_PASSWORD: string;

  @IsString()
  DB_NAME: string;

  @IsString()
  DB_USER: string;

  @IsNumberString()
  PORT: string;

  @IsString()
  NODE_ENV: Environment;
}

function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToInstance(EnvironmentVariables, config, {
    enableImplicitConversion: true,
  });
  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
}

export default validate;
