import { Module } from '@nestjs/common';
import { SpaceService } from './space.service';
import { SpaceController } from './space.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SpaceRepository } from './space.repository';

@Module({
  imports: [TypeOrmModule.forFeature([SpaceRepository])],
  controllers: [SpaceController],
  providers: [SpaceService, SpaceRepository],
  exports: [SpaceService, SpaceRepository],
})
export class SpaceModule {}
