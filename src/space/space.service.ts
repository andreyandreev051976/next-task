import { Injectable } from '@nestjs/common';
import { CreateSpaceDto } from './dto/create-space.dto';
import { UpdateSpaceDto } from './dto/update-space.dto';
import { SpaceRepository } from './space.repository';

@Injectable()
export class SpaceService {
  constructor(private readonly spaceRepository: SpaceRepository) {}
  async create(createSpaceDto: CreateSpaceDto) {
    const entity = this.spaceRepository.create(createSpaceDto);
    await this.spaceRepository.insert(entity);
    return entity;
  }

  findAll() {
    return this.spaceRepository.find();
  }

  findOne(id: string) {
    return this.spaceRepository.findExistingOne(id);
  }

  async update(id: string, updateSpaceDto: UpdateSpaceDto) {
    await this.spaceRepository.findExistingOne(id);
    this.spaceRepository.update({ id }, updateSpaceDto);
    return this.spaceRepository.findExistingOne(id);
  }

  async remove(id: string) {
    await this.spaceRepository.findExistingOne(id);
    this.spaceRepository.delete({ id });
    return true;
  }
}
