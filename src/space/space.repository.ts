import { DataSource, Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { Space } from './entities/space.entity';

@Injectable()
export class SpaceRepository extends Repository<Space> {
  constructor(private readonly dataSource: DataSource) {
    super(Space, dataSource.createEntityManager());
  }

  async findExistingOne(id: string) {
    const target = await this.findOne({ where: { id } });
    if (!target) {
      throw new NotFoundException();
    }

    return target;
  }
}
