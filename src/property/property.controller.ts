import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Delete,
  Post,
} from '@nestjs/common';
import { PropertyTemplatesService } from './property.service';
import { UpdatePropertyTemplateDto } from './dto/update-property.dto';
import { ApiTags } from '@nestjs/swagger';
import { CreatePropertyTemplateDto } from './dto/create-property.dto';
import { FilterProperty } from './dto/filter-property.dto';

@ApiTags('property-templates')
@Controller('property-templates')
export class PropertyTemplatesController {
  constructor(
    private readonly propertyTemplatesService: PropertyTemplatesService,
  ) {}

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.propertyTemplatesService.findOne(id);
  }

  @Get()
  findAll() {
    return this.propertyTemplatesService.findAll();
  }

  @Post()
  create(@Body() dto: CreatePropertyTemplateDto) {
    return this.propertyTemplatesService.create(dto);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updatePropertyDto: UpdatePropertyTemplateDto,
  ) {
    return this.propertyTemplatesService.update(id, updatePropertyDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.propertyTemplatesService.remove(id);
  }

  @Post('/search')
  search(@Body() dto: FilterProperty[]) {
    return this.propertyTemplatesService.search(dto);
  }
}
