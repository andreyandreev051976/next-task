import { DataSource, Repository } from 'typeorm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { Property } from './entities/property.entity';
import { PropertyTemplates } from './entities/property-templates.entity';

@Injectable()
export class PropertyRepository extends Repository<Property> {
  constructor(private readonly dataSource: DataSource) {
    super(Property, dataSource.createEntityManager());
  }

  async isTableExists(tableName: string) {
    const result = await this.query(
      `
      SELECT WHERE EXISTS (SELECT 1
        FROM   information_schema.tables 
        WHERE  table_name = $1)
    `,
      [tableName],
    );

    return result.length;
  }

  async isColumnExists(tableName: string, column: string) {
    const result = await this.query(
      `
      SELECT WHERE EXISTS (SELECT 
        FROM   information_schema.columns 
        WHERE  table_name = $1
        AND column_name = $2)
    `,
      [tableName, column],
    );

    return result.length;
  }

  async findExistingOne(id: string) {
    const target = await this.findOne({ where: { id } });
    if (!target) {
      throw new NotFoundException();
    }

    return target;
  }
}

@Injectable()
export class PropertyTemplatesRepository extends Repository<PropertyTemplates> {
  constructor(private readonly dataSource: DataSource) {
    super(PropertyTemplates, dataSource.createEntityManager());
  }

  async isTableExists(tableName: string) {
    const result = await this.query(
      `
      SELECT WHERE EXISTS (SELECT 1
        FROM   information_schema.tables 
        WHERE  table_name = $1)
    `,
      [tableName],
    );

    return result.length;
  }

  async isColumnExists(tableName: string, column: string) {
    const result = await this.query(
      `
      SELECT WHERE EXISTS (SELECT 
        FROM   information_schema.columns 
        WHERE  table_name = $1
        AND column_name = $2)
    `,
      [tableName, column],
    );

    return result.length;
  }

  async findExistingOne(id: string) {
    const target = await this.findOne({ where: { id } });
    if (!target) {
      throw new NotFoundException();
    }

    return target;
  }
}
