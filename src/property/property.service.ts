import { BadRequestException, Injectable } from '@nestjs/common';
import {
  CreatePropertyDto,
  CreatePropertyFromTemplateDto,
  CreatePropertyTemplateDto,
} from './dto/create-property.dto';
import {
  UpdatePropertyDto,
  UpdatePropertyTemplateDto,
} from './dto/update-property.dto';
import {
  PropertyRepository,
  PropertyTemplatesRepository,
} from './property.repository';
import { ResourceService } from 'src/resource/resource.service';
import {
  IDefinition,
  TYPES_NAME,
  getType,
  validateValueByDefinition,
} from './utils/type';
import { Property } from './entities/property.entity';
import { FindManyOptions } from 'typeorm';
import { SearchProperty } from './search.service';
import { FilterProperty } from './dto/filter-property.dto';

@Injectable()
export class PropertyTemplatesService {
  constructor(
    private readonly propertyTemplateRepository: PropertyTemplatesRepository,
    private readonly resourceService: ResourceService,
    private readonly searchService: SearchProperty,
  ) {}
  async validateDefinition(value: CreatePropertyTemplateDto) {
    if (!value) {
      return;
    }
    const { type, tableKey, tableName, isExternal } = value;
    const isTypeDefined = type !== undefined;
    if (!isTypeDefined) {
      throw new BadRequestException(`Set type.`);
    }
    const typeScheme = getType(type);
    if (!typeScheme) {
      throw new BadRequestException(
        `Invalid type. Allowed: "${TYPES_NAME}". Get "${type}"`,
      );
    }
    if (!tableKey || !tableName || isExternal) {
      return true;
    }
    const isTableExist =
      await this.propertyTemplateRepository.isTableExists(tableName);
    if (!isTableExist) {
      throw new BadRequestException('there is no such table');
    }
    const isColumnExist = await this.propertyTemplateRepository.isColumnExists(
      tableName,
      tableKey,
    );
    if (!isColumnExist) {
      throw new BadRequestException('there is no such column');
    }
    return true;
  }

  async create({ resource, ...createPropertyDto }: CreatePropertyTemplateDto) {
    await this.validateDefinition(createPropertyDto);
    const entity = this.propertyTemplateRepository.create(createPropertyDto);
    await this.propertyTemplateRepository.insert(entity);
    if (resource) {
      const resourceEntity = await this.resourceService.create(resource);
      resourceEntity.property = entity;
      await this.resourceService.update(resourceEntity.id, resourceEntity);
    }
    return entity;
  }

  findAll() {
    return this.propertyTemplateRepository.find();
  }

  findOne(id: string) {
    return this.propertyTemplateRepository.findExistingOne(id);
  }

  async findTemplate(templateId: string) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { id, ...entity } =
      await this.propertyTemplateRepository.findExistingOne(templateId);
    return entity;
  }

  async update(
    id: string,
    { resource, ...updatePropertyDto }: UpdatePropertyTemplateDto,
  ) {
    const entity = await this.propertyTemplateRepository.findExistingOne(id);
    let resourceEntity = entity.resource;
    if (resource) {
      resourceEntity = await this.resourceService.create(resource);
    }
    await this.validateDefinition({ ...entity, ...updatePropertyDto });
    await this.propertyTemplateRepository.update(
      { id },
      { resource: resourceEntity, ...updatePropertyDto },
    );
    return this.findOne(id);
  }

  async remove(id: string) {
    await this.propertyTemplateRepository.findExistingOne(id);
    this.propertyTemplateRepository.delete(id);
    return true;
  }

  search(filters: FilterProperty[]) {
    return this.searchService.filterBy(filters);
  }
}

@Injectable()
export class PropertyService {
  constructor(
    private readonly propertyRepository: PropertyRepository,
    private readonly propertyTemplateService: PropertyTemplatesService,
  ) {}

  private validateValue(value: any, definition: IDefinition) {
    return validateValueByDefinition(value, definition);
  }

  async create({ templateId, ...dto }: CreatePropertyDto) {
    const entity = await this.createFromTemplate(templateId, dto);
    await this.propertyRepository.insert(entity);
    return entity;
  }

  findAll() {
    return this.propertyRepository.find();
  }

  findOne(id: string) {
    return this.propertyRepository.findExistingOne(id);
  }

  findOneBy(options: FindManyOptions<Property>) {
    return this.propertyRepository.findOne(options);
  }

  async update(id: string, { value, ...updatePropertyDto }: UpdatePropertyDto) {
    const property = await this.findOne(id);
    if (value !== undefined) {
      const isValid = this.validateValue(value, getType(property.type));
      if (!isValid) {
        return new BadRequestException('Invalid value');
      }
    }
    await this.propertyRepository.update(
      { id },
      { value, ...updatePropertyDto },
    );
    return this.findOne(id);
  }

  async remove(id: string) {
    await this.propertyRepository.findExistingOne(id);
    this.propertyRepository.delete(id);
    return true;
  }

  private async createFromTemplate(
    id: string,
    { value, ...dto }: CreatePropertyFromTemplateDto,
  ) {
    const template = await this.propertyTemplateService.findTemplate(id);
    const isValid = this.validateValue(value, getType(template.type));
    if (!isValid) {
      throw new BadRequestException('Invalid value');
    }
    return this.propertyRepository.create({
      ...template,
      templateId: id,
      value,
      ...dto,
    });
  }
}
