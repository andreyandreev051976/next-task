import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { PropertyTemplates } from './property-templates.entity';
import { Task } from 'src/task/entities/task.entity';

@Entity()
export class Property extends PropertyTemplates {
  @ManyToOne(() => Task, (task) => task.properties, {
    orphanedRowAction: 'delete',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn()
  task: Task;

  @Column({
    type: 'varchar',
    length: 255,
  })
  templateId: string;
}
