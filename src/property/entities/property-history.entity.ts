import { Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import {
  HistoryActionColumn,
  HistoryActionType,
  HistoryFor,
  SnapshotColumn,
  InitiatorColumn,
} from 'src/history';
import { Property } from './property.entity';

@Entity()
@HistoryFor(Property)
export class PropertyHistory {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @SnapshotColumn({ type: 'jsonb' })
  payload: Property;

  @HistoryActionColumn()
  action: HistoryActionType;

  @UpdateDateColumn()
  updateAt: Date;

  @InitiatorColumn()
  userId: number;
}
