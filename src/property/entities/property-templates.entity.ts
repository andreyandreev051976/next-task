import { Resource } from 'src/resource/entities/resource.entity';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class PropertyTemplates {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(() => Resource, (resource) => resource.property, {
    eager: true,
    cascade: true,
  })
  resource: Resource | null;

  @Column({
    type: 'text',
    nullable: true,
  })
  type: string | null;

  @Column({
    type: 'text',
    nullable: true,
  })
  tableName: string | null;

  @Column({
    type: 'text',
    nullable: true,
  })
  tableKey: string | null;

  @Column({
    type: 'boolean',
    nullable: true,
  })
  isExternal: boolean;

  @Column({
    type: 'jsonb',
    nullable: true,
  })
  value: string | null;

  @Column({
    type: 'text',
    nullable: true,
  })
  name: string | null;
}
