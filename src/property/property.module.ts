import { Module } from '@nestjs/common';
import { PropertyService, PropertyTemplatesService } from './property.service';
import { PropertyTemplatesController } from './property.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  PropertyRepository,
  PropertyTemplatesRepository,
} from './property.repository';
import { PropertyTemplates } from './entities/property-templates.entity';
import { Property } from './entities/property.entity';
import { ResourceModule } from 'src/resource/resource.module';
import { SearchProperty } from './search.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Property, PropertyTemplates]),
    ResourceModule,
  ],
  controllers: [PropertyTemplatesController],
  providers: [
    PropertyRepository,
    PropertyService,
    PropertyTemplatesService,
    PropertyTemplatesRepository,
    SearchProperty,
  ],
  exports: [
    PropertyRepository,
    PropertyService,
    PropertyTemplatesService,
    PropertyTemplatesRepository,
    SearchProperty,
  ],
})
export class PropertyModule {}
