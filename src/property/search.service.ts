import { Injectable } from '@nestjs/common';
import { PropertyRepository } from './property.repository';
import { FilterProperty } from './dto/filter-property.dto';

@Injectable()
export class SearchProperty {
  constructor(private readonly propertyRepository: PropertyRepository) {}
  filterBy(filters: FilterProperty[]) {
    const where = filters
      .map(
        ({ id, value }) =>
          `p.value @> '${JSON.stringify(value)}'::jsonb AND p.id = '${id}'`,
      )
      .join(' OR ');
    return this.propertyRepository.query(
      `SELECT t.*, coalesce(jsonb_agg(p.*), '[]') properties 
        FROM public.task t
        LEFT JOIN public.property p ON p."taskId" = t.id
        ${where ? `WHERE ${where}` : ''}
        GROUP BY t.id
        `,
    );
  }
}
