const TYPES = {
  ARRAY: {
    selector: /array\[(?<type>.+)\]/,
    type: 'array',
  },
  ENUM: {
    selector: /(?<enum>[^,]+)(?=,|(?<=.+,.+)$)/g,
    type: 'enum',
  },
  NUMBER: {
    selector: /number/,
    type: 'number',
  },
  STRING: {
    selector: /string/,
    type: 'string',
  },
  BOOLEAN: {
    selector: /boolean/,
    type: 'boolean',
  },
} as const;

export const LIST_TYPES = Object.values(TYPES);
export const TYPES_NAME = Object.keys(TYPES);

type TNumber = (typeof TYPES)['NUMBER'];
type TString = (typeof TYPES)['STRING'];
type TBoolean = (typeof TYPES)['STRING'];
type TEnum = (typeof TYPES)['ENUM'] & { subtypes: Array<string> };
type TArray = (typeof TYPES)['ENUM'] & { subtypes: Array<IDefinition> };

export type IDefinition = TNumber | TString | TBoolean | TEnum | TArray;

export function getType(type: string): IDefinition | undefined {
  for (const config of LIST_TYPES) {
    const { selector } = config;
    const is = type.match(selector);
    if (is && config) {
      const { groups, index } = is;
      const isGlobal = index === undefined;

      return {
        ...config,
        subtypes:
          getSubtype(groups) || (isGlobal ? (is as Array<string>) : undefined),
      } as IDefinition;
    }
  }
}

function getSubtype(type: Record<string, string>) {
  if (!type) {
    return undefined;
  }
  const keys = Object.keys(type);
  return keys.reduce<Array<ReturnType<typeof getType>>>((subtype, typeKey) => {
    subtype.push(getType(type[typeKey]));
    return subtype;
  }, []);
}

const PRIMITIVES = { number: 1, string: 1, boolean: 1 };
const ENUM = { enum: 1 };
const ARRAY = { array: 1 };

export function validateValueByDefinition(value: any, definition: IDefinition) {
  if (!definition) {
    return true;
  }
  const { type } = definition;
  if (PRIMITIVES[type]) {
    return typeof value === type;
  }
  if (ENUM[type]) {
    const { subtypes } = definition as TEnum;
    return subtypes?.includes(value) || false;
  }
  if (ARRAY[type]) {
    const { subtypes } = definition as TArray;
    return (
      Array.isArray(value) &&
      value.every((e) => validateValueByDefinition(subtypes[0], e))
    );
  }
  return false;
}
