import { ApiProperty } from '@nestjs/swagger';
import { CreateResourceDto } from 'src/resource/dto/create-resource.dto';

export class CreatePropertyDto {
  @ApiProperty()
  value: string | null;
  @ApiProperty()
  templateId: string;
}

export class CreatePropertyTemplateDto {
  @ApiProperty()
  tableKey?: string | null;
  @ApiProperty()
  tableName?: string | null;
  @ApiProperty()
  enum?: string | null;
  @ApiProperty()
  type: string | null;
  @ApiProperty()
  isExternal: boolean | null;
  @ApiProperty()
  name: string | null;
  @ApiProperty()
  resource?: CreateResourceDto | null;
}

export class CreatePropertyFromTemplateDto {
  @ApiProperty()
  value: string | null;
}
