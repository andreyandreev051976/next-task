import { OmitType, PartialType } from '@nestjs/swagger';
import {
  CreatePropertyDto,
  CreatePropertyTemplateDto,
} from './create-property.dto';

export class UpdatePropertyDto extends PartialType(
  OmitType(CreatePropertyDto, ['templateId']),
) {}

export class UpdatePropertyTemplateDto extends PartialType(
  CreatePropertyTemplateDto,
) {}
