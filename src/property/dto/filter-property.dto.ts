import { ApiProperty } from '@nestjs/swagger';

export class FilterProperty {
  @ApiProperty()
  id: string;

  @ApiProperty()
  value: Record<string, any>;
}
