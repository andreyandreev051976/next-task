import { Module } from '@nestjs/common';

import { ConfigModule } from '@nestjs/config';
import setupConfiguration from './setup/configuration/configuration';
import { DatabaseModule } from './setup/database/database.module';
import { VariablesModule } from './setup/variables/variables.module';
import { TaskModule } from './task/task.module';
import { PropertyModule } from './property/property.module';
import { MediaModule } from './media/media.module';
import { TypeOrmHistoryModule } from './history';
import { DataSource } from 'typeorm';
import { SpaceModule } from './space/space.module';
import { ClsModule } from 'nestjs-cls';
import { Request } from 'express';
import { ACCESS_TOKEN_KEY } from './roles/roles.guard';
import { decode } from 'jsonwebtoken';
import { USER_KEY } from './app.constants';
import { UserModule } from './user/user.module';
import { UserTypeModule } from './user-type/user-type.module';
import { ResourceModule } from './resource/resource.module';

@Module({
  imports: [
    ConfigModule.forRoot(setupConfiguration()),
    TypeOrmHistoryModule.registerAsync({
      inject: [DataSource],
      useFactory: (connection: DataSource) => ({
        connection,
      }),
    }),
    ClsModule.forRoot({
      global: true,
      middleware: {
        mount: true,
        setup: (cls, req: Request) => {
          const authCookie = req.cookies[ACCESS_TOKEN_KEY];
          let user = null;
          if (authCookie) {
            user = decode(authCookie);
          }
          cls.set(USER_KEY, user);
        },
      },
    }),
    VariablesModule,
    DatabaseModule,
    TaskModule,
    PropertyModule,
    MediaModule,
    SpaceModule,
    UserModule,
    UserTypeModule,
    ResourceModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
