import { SetMetadata, UseGuards, applyDecorators } from '@nestjs/common';
import { ROLES_KEY } from './roles-auth.decorator';
import { AuthGuard } from './authorized.guard';
import { RolesGuard } from './roles.guard';

const applyIf = <V>(condition: boolean, value: V) => (condition ? [value] : []);

export function Auth(roles?: string[]) {
  const hasRoles = roles && roles.length !== 0;
  return applyDecorators(
    ...applyIf(hasRoles, SetMetadata(ROLES_KEY, roles)),
    UseGuards(AuthGuard, ...applyIf(hasRoles, RolesGuard)),
  );
}
