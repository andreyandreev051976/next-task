import { Module } from '@nestjs/common';
import { MediaService } from './media.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MediaRepository } from './media.repository';

@Module({
  imports: [TypeOrmModule.forFeature([MediaRepository])],
  providers: [MediaRepository, MediaService],
  exports: [MediaRepository, MediaService],
})
export class MediaModule {}
