import { Injectable } from '@nestjs/common';
import { CreateMediaDto } from './dto/create-media.dto';
import { UpdateMediaDto } from './dto/update-media.dto';
import { MediaRepository } from './media.repository';

@Injectable()
export class MediaService {
  constructor(private readonly mediaRepository: MediaRepository) {}
  async create(createMediaDto: CreateMediaDto) {
    const mediaInstance = this.mediaRepository.create(createMediaDto);
    await this.mediaRepository.insert(mediaInstance);
    return mediaInstance;
  }

  async findOne(id: string) {
    return await this.mediaRepository.findOne({ where: { id } });
  }

  async update(id: string, updateMediaDto: UpdateMediaDto) {
    await this.mediaRepository.update({ id }, updateMediaDto);
    return this.findOne(id);
  }

  remove(id: string) {
    return this.mediaRepository.delete({ id });
  }
}
