import { Column } from 'typeorm';
import { TYPEORM_HISTORY_INITIATOR_COLUMN } from './history.constants';

export function InitiatorColumn(): AnyFn {
  return (target: any, key: any) => {
    Reflect.defineMetadata(TYPEORM_HISTORY_INITIATOR_COLUMN, key, target);
    return Column({
      type: 'int4',
      nullable: true,
    })(target, key);
  };
}
