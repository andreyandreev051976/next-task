import { Inject, Injectable } from '@nestjs/common';
import { EntitySubscriberInterface, getMetadataArgsStorage } from 'typeorm';
import { createHistorySubscriber } from './history.subscriber';
import {
  TYPEORM_HISTORY_HISTORY_FOR,
  TYPEORM_HISTORY_OPTIONS,
} from './history.constants';
import { TypeOrmHistoryModuleOptions } from './history.interface';
import { ClsService } from 'nestjs-cls';

const createSubscriber = (
  entity: AnyFn,
  historyEntity: AnyFn,
  cls: ClsService,
): EntitySubscriberInterface<any> => {
  return createHistorySubscriber(entity, historyEntity, cls);
};

@Injectable()
export class HistoryService {
  constructor(
    @Inject(TYPEORM_HISTORY_OPTIONS) options: TypeOrmHistoryModuleOptions,
    private readonly cls: ClsService,
  ) {
    const entities = getMetadataArgsStorage().tables.reduce((acc, t) => {
      const e = Reflect.getMetadata(TYPEORM_HISTORY_HISTORY_FOR, t.target);
      if (e !== undefined) {
        acc.push({
          entity: e,
          history: t.target,
        });
      }
      return acc;
    }, [] as any[]);
    entities
      .map(({ entity, history }) => {
        return createSubscriber(entity, history, cls);
      })
      .forEach((subscriber) => options.connection.subscribers.push(subscriber));
  }
}
