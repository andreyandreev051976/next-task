import { Abstract, ModuleMetadata, Provider, Type } from '@nestjs/common';
import { DataSource, EntityManager, EntitySubscriberInterface } from 'typeorm';

export interface HistoryEntityMapping {
  entity: AnyFn;
  history: AnyFn;
}

export interface TypeOrmHistoryModuleOptions {
  connection: DataSource;
}

export interface HistoryEntitySubscriberInterface<E, H>
  extends EntitySubscriberInterface<E> {
  entity: AnyFn;
  historyEntity: AnyFn;

  createHistoryEntity(manager: EntityManager, entity: E): H | Promise<H>;

  beforeInsertHistory(history: H): H | Promise<H>;

  afterInsertHistory(history: H): void | Promise<void>;

  beforeUpdateHistory(history: H): H | Promise<H>;

  afterUpdateHistory(history: H): void | Promise<void>;

  beforeRemoveHistory(history: H): H | Promise<H>;

  afterRemoveHistory(history: H): void | Promise<void>;
}

export enum HistoryActionType {
  Created = 'CREATED',
  Updated = 'UPDATED',
  Deleted = 'DELETED',
}

export interface TypeOrmHistoryOptionsFactory {
  createOptions():
    | Promise<TypeOrmHistoryModuleOptions>
    | TypeOrmHistoryModuleOptions;
}

export interface TypeOrmHistoryModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  inject?: Array<Type<any> | string | symbol | Abstract<any> | AnyFn>;
  extraProviders?: Provider[];
  useExisting?: Type<TypeOrmHistoryOptionsFactory>;
  useClass?: Type<TypeOrmHistoryOptionsFactory>;
  useFactory?: (
    ...args: any
  ) => TypeOrmHistoryModuleOptions | Promise<TypeOrmHistoryModuleOptions>;
}
