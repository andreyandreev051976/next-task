import { Column } from 'typeorm';
import { TYPEORM_HISTORY_ACTION_COLUMN } from './history.constants';
import { HistoryActionType } from './history.interface';

export function HistoryActionColumn(): AnyFn {
  return (target: any, key: any) => {
    Reflect.defineMetadata(TYPEORM_HISTORY_ACTION_COLUMN, key, target);
    return Column({
      default: HistoryActionType.Created,
      enum: Object.values(HistoryActionType),
      type: 'enum',
    })(target, key);
  };
}
