import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { map } from 'rxjs';
import { Variables } from 'src/setup/variables/variables.service';
import * as FormData from 'form-data';

@Injectable()
export class FileClient {
  constructor(
    private readonly httpService: HttpService,
    private readonly variablesService: Variables,
  ) {}

  private STORAGE = 'next-task';

  uploadFile(file: Express.Multer.File) {
    const endpoint = `${this.variablesService.FILE_CLIENT}/file/${this.STORAGE}/upload`;
    const formData = new FormData();
    formData.append('file', file.buffer, {
      filename: Buffer.from(file.originalname, 'latin1').toString('utf8'),
      contentType: file.mimetype,
    });
    return this.httpService.post(endpoint, formData).pipe(
      map((response: AxiosResponse) => {
        const { data } = response;
        return data;
      }),
    );
  }

  deleteFile(id: string) {
    const endpoint = `${this.variablesService.FILE_CLIENT}/file/${this.STORAGE}/${id}`;
    return this.httpService.delete(endpoint).pipe(
      map((response: AxiosResponse) => {
        const { data } = response;
        return data;
      }),
    );
  }
}
