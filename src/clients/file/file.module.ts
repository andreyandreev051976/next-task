import { Module } from '@nestjs/common';
import { FileClient } from './file.client';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: () => ({
        maxRedirects: 5,
      }),
    }),
  ],
  providers: [FileClient],
  exports: [FileClient],
})
export class FileModule {}
