import { Module } from '@nestjs/common';
import { AuthClient } from './auth.client';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000,
        maxRedirects: 5,
      }),
    }),
  ],
  providers: [AuthClient],
  exports: [AuthClient],
})
export class AuthModule {}
