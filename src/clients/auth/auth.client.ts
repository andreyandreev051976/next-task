import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { Observable, map } from 'rxjs';
import { Variables } from 'src/setup/variables/variables.service';

@Injectable()
export class AuthClient {
  constructor(
    private readonly httpService: HttpService,
    private readonly variablesService: Variables,
  ) {}

  private cache = new Map();
  private INVALIDATION_TIME = 1000 * 60;

  private setCache(key: number, value: object) {
    this.cache.set(key, { lastCall: Date.now(), instance: value });
  }

  getById(id: number): Observable<AxiosResponse<object>> {
    const entityFromCache = this.cache.get(id);
    if (entityFromCache) {
      if (entityFromCache.lastCall + this.INVALIDATION_TIME <= Date.now()) {
        return new Observable((subscriber) => {
          subscriber.next(this.cache.get(id));
          subscriber.complete();
        });
      }
    }
    const endpoint = `${this.variablesService.AUTH_SERVICE}/users/${id}`;
    return this.httpService.get(endpoint).pipe(
      map((response: AxiosResponse) => {
        const { data } = response;
        this.setCache(id, data);
        return data;
      }),
    );
  }

  hasById(id: number): Observable<boolean> {
    const entityFromCache = this.cache.get(id);
    if (entityFromCache) {
      if (entityFromCache.lastCall + this.INVALIDATION_TIME <= Date.now()) {
        return new Observable((subscriber) => {
          subscriber.next(true);
          subscriber.complete();
        });
      }
    }
    const endpoint = `${this.variablesService.AUTH_SERVICE}/users/${id}`;
    return this.httpService.get(endpoint).pipe(
      map((response: AxiosResponse) => {
        const isValid = response.status < 400;
        if (!isValid) {
          this.cache.delete(id);
        }
        this.setCache(id, response.data);
        return isValid;
      }),
    );
  }
}
